# **常用Git命令**

git clone  # 拉取

git rebase  # 变基

git pull  # 同步

git status  # 获取状态

git add .  # 将修改内容添加至暂存区

git commit  # 提交

git push  # 推送

\# git pull 同步命令详解

git pull 参数一 参数二 参数三

参数一: 同步的上游源仓库, 可选(upstream)与(origin). upstream(上游地址); origin(源地址) 

参数二: 同步的上游源仓库分支

参数三: 同步的目标仓库分支

例1: git pull upstream develop:origin/develop  # 同步上游仓库develop分支至源仓库develop分支

例2: git pull origin develop:develop  # 同步上游仓库develop分支至本地仓库develop分支

\# git push 推送命令详解

git push 参数一 参数二 参数三

参数一: 推送的目标仓库, 可选(upstream)与(origin). upstream(上游地址); origin(源地址) 

参数二: 推送的本地分支

参数三: 推送的目标分支

例1: git push origin develop:develop  # 推送本地develop分支至源地址develop分支

# **实际操作步骤**

## **拉取代码并配置相关参数**

\# 最开始的时候或者删除仓库后进行的操作 

```python
# 最开始的时候或者删除仓库后进行的操作

git clone https://gitee.com/tang-dawei/python
cd python/
git status
git config user.name "tang-dawei"
git config user.email hfuttdw@163.com
git remote remove origin
git remote add origin https://gitee.com/tang-dawei/python
git config user.name
git config user.name
git remote -v


```

**git clone https://gitee.com/tang-dawei/Community-document # 获取资料到本地, 正常返回如下:** 

​    Cloning into 'Community-document'...

​    remote: Enumerating objects: 4246, done.

​    remote: Counting objects: 100% (4246/4246), done.

​    remote: Compressing objects: 100% (2144/2144), done.

​    remote: Total 4246 (delta 1675), reused 4213 (delta 1657), pack-reused 0

​    Receiving objects: 100% (4246/4246), 175.67 MiB | 3.00 MiB/s, done.

​    Resolving deltas: 100% (1675/1675), done.

​    Updating files: 100% (1891/1891), done.

**cd Community-document/  # 切换路径**

​    \# pwd 参看当前目录

**git status  # 查看状态, 正常返回如下:** 

​    On branch develop

​    Your branch is up to date with 'origin/develop'.

​    nothing to commit, working tree clean

**git config user.name  # 查询用户名**

如果返回自己的仓库名正确（**是否正确可以按照下面的配置步骤进行比对，正确与否都可以直接配置，会默认覆盖**），则不需要设置； 如果没有返回自己仓库的名字, 则继续如下操作: 

​    git config --global user.name "你的名字(请使用英文)"  # 例:     git config user.name "tang-dawei"

![查询用户名](pictures/查询用户名.png)

​    git config --global user.email "邮箱账号"  # 例: git config user.email [hfuttdw@163.com](mailto:hfuttdw@163.com)

![查询邮箱名](pictures/查询邮箱名.png)

**git remote -v  # 查看当前远程仓库**

​    \# 如果返回的自己仓库地址跟上游的仓库地址正确, 这不需要更改设置. 例返回如下信息: 

​    origin  https://gitee.com/tu-hongli/Community-document (fetch)

​    origin  https://gitee.com/tu-hongli/Community-document (push)

​    upstream        https://gitee.com/quecpython/Community-document (fetch)

​    upstream        https://gitee.com/quecpython/Community-document (push)

​    \# 如果返回的自己仓库地址跟上游的仓库地址不正确, 则执行如下命令: 

​    \# 正常执行添加上游地址命令即可, 添加后可重新查看远程仓库状态. 

​    **git remote remove origin  # 删除源地址**

​    **git remote add origin https://gitee.com/tang-dawei/Community-document  # 添加源地址**

​    **git remote remove upstream  # 删除上游地址**

​    **git remote add upstream https://gitee.com/quecpython/Community-document  # 添加上游地址**

​    

**git pull upstream develop:develop  # 同步上游仓库develop分支至本地develop分支**

## **编辑提交过程**

**修改好代码后进行如下步骤**

```python
git status  # 获取状态, 查看是否有需要提交的内容

git add .  # 将修改内容添加至暂存区

git commit  # 提交更新, 根据下述添加提交修改信息
    # 1.先ESC健切换到编辑模式
    # 2.上下键(↑↓)切换到修改内容的最后一行
    # 3.字母A按键切换编写模式, 删除这一行首的#键, 然后输入“修改内容的简介”
    # 4.ESC键退出
    # 5.Shift + “:”键, 将鼠标切换到最后一行(如果无法输入, 切换输入即可)
    # 6.在最后一行输入: “:wq”, ENTER键退出
    # (这一部会用到vim 编辑器，退出前，你需要添加一些信息。在最后一行输入一些注释就行.)然后: wq退出

git push origin master:master  # 推送至远端仓库
```

```python
#关于Vim编辑器的相关说明
    # 在 Linux 中使用 vim 时, 输入 vim xxx.file 输入好文件内容之后, 怎么保存呢? 
    # 按 ESC, 左下角就可以进行输入(添加注释前需要敲此命令, 结束也要敲此命令) 
    # :w 保存但不退出
    # :wq 保存并退出
    # :q 退出
    # :q! 强制退出, 不保存
    # :e! 放弃所有修改, 从上次保存文件开始再编辑命令历史
```


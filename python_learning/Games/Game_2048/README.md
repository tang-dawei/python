# Introduction
https://mp.weixin.qq.com/s/WJhg4J0MuuEcmDasRzuE9Q

# Environment
```
OS: Windows10
Python: Python3.5+(have installed necessary dependencies)
```

# Usage

```
Step1:
pip install -r requirements.txt #可以使用git bash进入指定路径，然后下载安装，如下图
```

![pip安装requirement](test_pictures/pip安装requirement.jpg)





```
Step2:
run "python Game23.py" #使用vscode运行会出现路径错误，目前使用直接贴路径解决，获取路径无法正确的运行路径，带解决
```



**首次运行源码报错如下：**

**![报路径错误](test_pictures/报路径错误.jpg)**

**针对实际的MP3和font路径修改了cfg.py文件，原cfg文件命名为“cfg_old.py”**





# Game Display
![giphy](demonstration/running.gif)
'''配置文件'''
import os


'''FPS'''
FPS = 60
'''背景颜色'''
BG_COLOR = '#92877d'
'''屏幕大小'''
SCREENSIZE = (650, 370)
'''保存当前最高分的文件'''
MAX_SCORE_FILEPATH = 'score'
'''字体路径'''
print(os.getcwd())
FONTPATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'resources\\font\\Gabriola.ttf')
# FONTPATH = os.path.join(os.getcwd(), 'resources/font/Gabriola.ttf')   #如果使用os.getcwd()的话，可以使用cmd方式运行
'''背景音乐路径'''
BGMPATH = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'resources\\audio\\bgm.mp3')
# BGMPATH = os.path.join(os.getcwd(), 'resources/audio/bgm.mp3')     #如果使用os.getcwd()的话，可以使用cmd方式运行
'''其他一些必要的常量'''
MARGIN_SIZE = 10
BLOCK_SIZE = 80
GAME_MATRIX_SIZE = (4, 4)
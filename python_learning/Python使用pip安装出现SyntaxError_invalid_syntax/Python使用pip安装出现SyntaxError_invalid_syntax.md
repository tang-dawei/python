1. **首先使用pip安装第三方库的时候，习惯先输入python查看一下第三方库，导致直接进入了python的交互界面，只能输入python命令，无法pip安装第三方库**

   ![pip报错](pip报错.jpg)

2. **pip安装第三方库正确的命令是“pip install 第三方库名”，而不是“python pip install 第三方库名”**

![pip正确安装方式](pip正确安装方式.jpg)
import json

# dumps可以格式化所有的基本数据类型为字符串
data1 = json.dumps([])         # 列表
print(data1, type(data1))
data2 = json.dumps(2)          # 数字
print(data2, type(data2))
data3 = json.dumps('3')        # 字符串
print(data3, type(data3))
dict = {"amount":"2","content":[{"category_id":0,"name":"Classical literature","category_json_url":"http://172.16.242.14:8080/source/history/history.json"},{"category_id":1,"name":"music","category_json_url":"http://172.16.242.14:8080/source//popmusic/popmusic.json"}]}  # 字典

data4 = json.dumps(dict)
print(data4, type(data4))

with open("test.json", "w", encoding='utf-8') as f:
    # indent 超级好用，格式化保存字典，默认为None，小于0为零个空格
    f.write(json.dumps(dict, indent=4))
    json.dump(dict, f, indent=4)  # 传入文件描述符，和dumps一样的结果
    f.write(json.dumps(dict, indent=4))
    json.dump(dict, f, indent=4)  # 传入文件描述符，和dumps一样的结果
    

def stripFile(oldName,newName):
    # remove the space or Tab or enter in a file,and output to a new file in the same folder
    # 适用于不支持input的python
    with open(oldName, "r", encoding='utf-8') as fp:
        with open(newName, "w", encoding='utf-8') as newFp:
            for eachline in fp.readlines():
                newStr = eachline.replace(" ","").replace("\t","").strip()
                #print "Write:",newStr
                newFp.write(newStr)
            fp.close()
            newFp.close()

# oldName = input("input file name:")
oldName = "test.json"
nameList = oldName.split(".")
newName = "%s%s%s" % (nameList[0],"_new.",nameList[1])
stripFile(oldName,newName)

    
    
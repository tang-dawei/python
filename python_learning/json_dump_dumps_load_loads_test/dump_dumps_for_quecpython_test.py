import ujson
import uio

# dumps可以格式化所有的基本数据类型为字符串
data1 = ujson.dumps([])         # 列表
print(data1, type(data1))
data2 = ujson.dumps(2)          # 数字
print(data2, type(data2))
data3 = ujson.dumps('3')        # 字符串
print(data3, type(data3))
dict = {"name": "Tom", "age": 23}   # 字典
data4 = ujson.dumps(dict)
print(data4, type(data4))

with uio.open('/usr/test.json', mode='w') as f:
    # indent 超级好用，格式化保存字典，默认为None，小于0为零个空格
    f.write(ujson.dumps(dict)) # 书写一次文件
    ujson.dump(dict, f)  # 传入文件描述符，第二次书写文件，和dumps一样的结果，会导致两次同样的内容写了两次
    

import json

# dumps可以格式化所有的基本数据类型为字符串
data1 = json.dumps([])         # 列表
print(data1, type(data1))
data2 = json.dumps(2)          # 数字
print(data2, type(data2))
data3 = json.dumps('3')        # 字符串
print(data3, type(data3))
dict = {"amount":"2","content":[{"category_id":0,"name":"Classical literature","category_json_url":"http://172.16.242.14:8080/source/history/history.json"},{"category_id":1,"name":"music","category_json_url":"http://172.16.242.14:8080/source//popmusic/popmusic.json"}]}  # 字典

data4 = json.dumps(dict)
print(data4, type(data4))

with open("test.json", "w", encoding='utf-8') as f:
    # indent 超级好用，格式化保存字典，默认为None，小于0为零个空格
    f.write(json.dumps(dict, indent=4))
    json.dump(dict, f, indent=4)  # 传入文件描述符，和dumps一样的结果
    f.write(json.dumps(dict, indent=4))
    json.dump(dict, f, indent=4)  # 传入文件描述符，和dumps一样的结果
    f.close()
#至此会生成一个test.json文件，重复性写入多个内容，但是对于test.json文件不能直接使用loads进行操作，需要进行下面的函数配置        

def strip_and_enter_File(oldName,stripName,newName):
    with open(oldName, "r", encoding='utf-8') as fp:
        with open(stripName, "w", encoding='utf-8') as srtFp:
            for eachline in fp.readlines():
                fp_strip = eachline.replace(" ","").replace("\t","").strip() # fp_strip:remove the space or Tab or enter in a file,and output to a new file in the same folder
                srtFp.write(fp_strip)
            fp.close()
            srtFp.close()
        with open(stripName, "r", encoding='utf-8') as srtFp:
            with open(newName, "w", encoding='utf-8') as newFp:
                data = srtFp.read().replace('}{','}\r{') # 如果对于上面的fp_strip直接写进文件的话，会导致永远不换行，也无法使用json.loads操作，需要对不同字典进行换行操作（意思是一行一个字典）
                print(data)
                newFp.write(data)
            srtFp.close()
            newFp.close()
                
# oldName = input("input file name:")
oldName = "test.json"
nameList = oldName.split(".")
stripName = "%s%s%s" % (nameList[0],"_strip.",nameList[1])
newName = "%s%s%s" % (nameList[0],"_new.",nameList[1])
strip_and_enter_File(oldName, stripName ,newName)

with open(newName, 'r') as f:
    for line in f.readlines():
        json_data = json.loads(line)
        print("json_data:",json_data)

'''
def stripFile(oldFName,newFName):
    # remove the space or Tab or enter in a file,and output to a new file in the same folder
    # 适用于不支持input的python，例如：QuecPython
    with open("test_old.json", "r", encoding='utf-8') as fp:
        with open("test_new.json", "w", encoding='utf-8') as newFp:
            for eachline in fp.readlines():
                newStr = eachline.replace(" ","").replace("\t","").strip()
                newFp.write(newStr)
            fp.close()
            newFp.close()
if __name__ == "__main__":
    stripFile('test_old.json','test_new.json')
'''

''' 
def stripFile(oldName,newName):
    # remove the space or Tab or enter in a file,and output to a new file in the same folder
    # 适用于支持input的python
    with open(oldName, "r", encoding='utf-8') as fp:
        with open(newName, "w", encoding='utf-8') as newFp:
            for eachline in fp.readlines():
                newStr = eachline.replace(" ","").replace("\t","").strip()
                print(newStr)
                #print "Write:",newStr
                newFp.write(newStr)
            fp.close()
            newFp.close()
if __name__ == "__main__":
    oldName = input("input file name:")
    nameList = oldName.split(".")
    newName = "%s%s%s" % (nameList[0],"_new.",nameList[1])
    stripFile(oldName,newName)
'''
       

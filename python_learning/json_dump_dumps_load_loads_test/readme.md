# 为什么要学习用Python解析JSON数据？

“JSON(JavaScript Object Notation, JS 对象简谱) 是一种轻量级的数据交换格式。它基于 ECMAScript (欧洲计算机协会制定的js规范)的一个子集，采用完全独立于编程语言的文本格式来存储和表示数据。简洁和清晰的层次结构使得 JSON 成为理想的数据交换语言。易于人阅读和编写，同时也易于机器解析和生成，并有效地提升网络传输效率。”——《百度百科》

我们在爬取网站的时候，经常会遇到获取了html却没有在标签里找到数据的情况。这种情况大概率html只是一个框架，而数据是JavaScript脚本从服务器获取JSON数据后再把数据写入标签的。所以我们直接获取html会找不到需要的数据。解决的方法也不难，使用浏览器的开发者模式可以看到网页对服务器发出的请求，找到获取数据的请求后模拟该请求即可从服务器获取JSON数据。获取JSON数据后即可用json库转化为python的数据类型。

JSON ([JavaScript](https://so.csdn.net/so/search?from=pc_blog_highlight&q=JavaScript) Object Notation) 是一种轻量级的数据交换格式。[Python](https://so.csdn.net/so/search?from=pc_blog_highlight&q=Python)3 中可以使用 json 模块来对 JSON 数据进行编解码，它主要提供了四个方法： `dumps`、`dump`、`loads`、`load`。

这几个接口的常见应用可以参考同目录下的demo

# 常见的错误

读取多行的JSON文件
假如要读取一个多行的JSON文件：

```python
{"坂": ["坂5742"]}
{"构": ["构6784"]}
{"共": ["共5171"]}
{"钩": ["钩94a9"]}
{"肮": ["肮80ae"]}
{"孤": ["孤5b64"]}
```


如果直接使用：

```python
with open(json_path, 'r') as f:
    json_data = json.load(f)
```

就会报错：抛出异常JSONDecodeError。

```python
json.decoder.JSONDecodeError: Extra data: line 2 column 1 (char 17)
```

表示数据错误，数据太多，第2行第一列 
因为json只能读取一个文档对象，有两个解决办法 
1、单行读取文件,一次读取一行文件。 
2、保存数据源的时候，格式写为一个对象。

单行读取文件：

```python
    with open(json_path, 'r') as f:
        for line in f.readlines():
            json_data = json.loads(line)
```

但是这种做法还有个问题，如果JSON文件中包含空行，还是会抛出JSONDecodeError异常

但是这种做法还有个问题，如果JSON文件中包含空行，还是会抛出JSONDecodeError异常

```python
json.decoder.JSONDecodeError: Expecting value: line 2 column 1 (char 1)
```

可以先处理空行，再进行文件读取操作：

```python
 for line in f.readlines():
        line = line.strip()   # 使用strip函数去除空行
        if len(line) != 0:
            json_data = json.loads(line)
```


合并为一个对象
将json文件处理成一个对象文件。

```python
{"dict": [
{"坂": ["坂5742"]},
{"构": ["构6784"]},
{"共": ["共5171"]},
{"钩": ["钩94a9"]},
{"肮": ["肮80ae"]},
{"孤": ["孤5b64"]}
]}
```

然后再用：

然后再用：

```python
with open(json_path, 'r') as f:
    json_data = json.loads(f.read())
```
# dumps与loads的相互转换

经过dumps转换成文件后，会存在空格、换行等等，如下截图：

![json文件存在多个空格和换行](json文件存在多个空格和换行.jpg)

但是直接使用loads会报一系列的错误，思路是：

1. 先去除json文件里面所有的空格（会生成一个不换行的json文件）

2. 在对每个字典进行换行操作，结果如下：

   ![多行json](多行json.jpg)


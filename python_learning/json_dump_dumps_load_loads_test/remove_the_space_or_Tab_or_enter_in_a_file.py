'''
def stripFile(oldFName,newFName):
    # remove the space or Tab or enter in a file,and output to a new file in the same folder
    # 适用于不支持input的python，例如：QuecPython
    with open("test_old.json", "r", encoding='utf-8') as fp:
        with open("test_new.json", "w", encoding='utf-8') as newFp:
            for eachline in fp.readlines():
                newStr = eachline.replace(" ","").replace("\t","").strip()
                newFp.write(newStr)
            fp.close()
            newFp.close()
if __name__ == "__main__":
    stripFile('test_old.json','test_new.json')

'''
   
def stripFile(oldName,newName):
    # remove the space or Tab or enter in a file,and output to a new file in the same folder
    # 适用于不支持input的python
    with open(oldName, "r", encoding='utf-8') as fp:
        with open(newName, "w", encoding='utf-8') as newFp:
            for eachline in fp.readlines():
                newStr = eachline.replace(" ","").replace("\t","").strip()
                #print "Write:",newStr
                newFp.write(newStr)
            fp.close()
            newFp.close()
if __name__ == "__main__":
    oldName = input("input file name:")
    nameList = oldName.split(".")
    newName = "%s%s%s" % (nameList[0],"_new.",nameList[1])
    stripFile(oldName,newName)
    

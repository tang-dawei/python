os.path.abspath(__file__) 作用： 获取当前脚本的完整路径

```python
import os
print(os.path.abspath(__file__))
```



![result01](result01.jpg)





注意：

只有当在脚本中执行的时候，os.path.abspath(__file__)才会起作用，因为该命令是获取的当前执行脚本的完整路径，如果在交互模式或者terminate 终端中运行会报没有__file__这个错误:

```python
>>> import os
>>> cur_path = os.path.dirname(os.path.abspath(__file__))
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name '__file__' is not defined
```


import struct
import binascii
# values = (1, 'abc', 2.7)   如果对左侧values进行packed_data = s.pack(*values)的话，会报错“struct.error: argument for 's' must be a bytes object”
values = (1, b'abc', 1.22)
s = struct.Struct('I3sf')
packed_data = s.pack(*values)
unpacked_data = s.unpack(packed_data)
 
print('Original values:', values)
print('Format string :', s.format)
print('Uses :', s.size, 'bytes')
print('Packed Value :', binascii.hexlify(packed_data))
print('Unpacked Type :', type(unpacked_data), ' Value:', unpacked_data)


import struct
import binascii
import ctypes
 
values = (1, b'abc', 2.7)
s = struct.Struct('I3sf')
prebuffer = ctypes.create_string_buffer(s.size)
print('Before :',binascii.hexlify(prebuffer))
s.pack_into(prebuffer,0,*values)
print('After pack:',binascii.hexlify(prebuffer))
unpacked = s.unpack_from(prebuffer,0)
print('After unpack:',unpacked)


import struct
import binascii
import ctypes
 
values1 = (1, b'abc', 2.7)
values2 = (b'defg',101)
s1 = struct.Struct('I3sf')
s2 = struct.Struct('4sI')
 
prebuffer = ctypes.create_string_buffer(s1.size+s2.size)
print('Before :',binascii.hexlify(prebuffer))
s1.pack_into(prebuffer,0,*values1)
s2.pack_into(prebuffer,s1.size,*values2)
print('After pack:',binascii.hexlify(prebuffer))
print(s1.unpack_from(prebuffer,0))
print(s2.unpack_from(prebuffer,s1.size))



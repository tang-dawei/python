# 修订历史

| 版本 | 日期       | 作者  | 变更表述                       |
| ---- | ---------- | ----- | ------------------------------ |
| 1.0  | 2021-11-19 | David | 首次编写，仅介绍具体的操作说明 |

适用开发方式：QuecPython开发（Quecopen应该也适用，但是本文作者未测试）

编写初衷：由于多数客户遇到音频质量问题无法满足其需求，需要音频同事帮忙调节对应的nvm参数，然后调节好的nvm文件需要加载到customer_fs分区或者ReliableData分区（由于部分nvm文件放在customer_fs无法生效，所以本文仅介绍如何加载到ReliableData分区）

# 更改默认参数到RD文件



1. 更改一个默认参数首先需要将对应项目的ReliableData.bin文件用RDParser.exe工具解开

   **备注：RDParser工具已放置在此文档的同级目录下**

   解压说明如下：

   ![RDP解压说明](RDP解压说明.png)

   解压结果如下：

   ![RDP解压结果](RDP解压结果.jpg)

   

2. 使用CAT Studio的NvmEditor打开上面解开的某一项nv（对应调试音频参数，可以提JIRA给音频同事协助解决）

3. 修改“QuectelBlackFilterList.txt”文件

   注意如果RD中原本没有文件QuectelBlackFilterList.txt的话，请添加此文件，同时将版本号改为1，然后在此文件中添加刚刚更改的nv项的名称。如果已经存在此文件的话，请将版本号加1处理，同时保证刚刚更改的nv项包含在了QuectelBlackFilterList.txt黑名单中。（QuectelBlackFilterList.txt的第一行是版本号，不是文件数，所以说每次更新仅需要加1处理，无论新增几个文件），举例如下：

   原QuectelBlackFilterList.txt内容如下：

   ```
   QuecFilterListVer:1
   TTPCom_NRAM2_CUSTOMIZATION_DATA.gki
   TTPCom_NRAM2_CUSTOMIZATION_DATA_2.gki
   ```

   由于需要增加两个文件，那么修改后的QuectelBlackFilterList.txt内容如下：

   ```
   QuecFilterListVer:2
   TTPCom_NRAM2_CUSTOMIZATION_DATA.gki
   TTPCom_NRAM2_CUSTOMIZATION_DATA_2.gki
   audio_gain.nvm
   audio_ve.nvm
   ```

4. 将更新或者新增的nvm文件与原文件一起合并成ReliableData.bin，使用工具是“RDGenerator_3601.exe”

   **备注：RDGenerator_3601.exe工具已放置在此文档的同级目录下**

   压缩说明如下：

   - 先选中需要压缩的文件等信息，点击“generate”

     ![RD压缩说明1](RD压缩说明1.jpg)

   - 选择“是”

     ![RD压缩说明2](RD压缩说明2.jpg)

   - 选择”是“

     ![RD压缩说明3](RD压缩说明3.jpg)
   
     

压缩结果如下：

![RD压缩结果](RD压缩结果.jpg)
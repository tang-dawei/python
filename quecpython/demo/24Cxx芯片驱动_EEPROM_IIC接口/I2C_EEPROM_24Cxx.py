from machine import I2C
import utime
'''
I2C使用示例 
'''

# 参考 http://qpy.quectel.com/wiki/#/zh-cn/api/?id=i2c
# 设置日志输出级别


def test_i2c():
    i2c_dev = I2C(I2C.I2C1, I2C.STANDARD_MODE)
    addres = 0x50
    r_data = bytearray(10)  # 存储数据
    # 读写寄存器
    LIS2DH12_CTRL_REG2 = bytearray({0x20, 0}) #
    LIS2DH12_CTRL_REG3 = bytearray({0x25, 0})  #
    w_data = [0x30,0x31,0x32,0x33,0x34]  # 想要写的数据
    w1_data = [0x40,0x41,0x42,0x43,0x44]  # 想要写的数据
    i2c_dev.write(addres, LIS2DH12_CTRL_REG2, 1, bytearray(w_data), 5)
    i2c_dev.write(addres, LIS2DH12_CTRL_REG3, 1, bytearray(w1_data), 5)
    i2c_dev.read(addres, LIS2DH12_CTRL_REG2, 1, r_data, 10, 2)
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[0]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[1]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[2]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[3]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[4]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[5]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[6]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[7]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[8]))
    print("读出寄存器的数所为 0x{0:02x}".format(r_data[9]))
    print("退出")




if __name__ == "__main__":
    test_i2c()

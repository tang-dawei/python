import log
import utime
import _thread
from machine import Pin
from machine import UART

log.basicConfig(level=log.NOTSET)  # 设置日志输出级别
log = log.getLogger("Grey")  # 获取logger对象，如果不指定name则返回root对象，多次使用相同的name调用getLogger方法返回同一个logger对象

# GNSS模组L76_K常用设置
# 1-> $PCAS01,1*1D<CR><LF>  //设置模块波特率9600
# 2-> $PCAS01,5*19<CR><LF>  //设置模块波特率115200
# 3-> $PCAS02,200*1D<CR><LF>  //设置吐定位信息频率为5Hz
# 4-> $PCAS02,500*1A<CR><LF>  //设置吐定位信息频率为2Hz
# 5-> $PCAS02,1000*2E<CR><LF>  //设置吐定位信息频率为1Hz

# 初始波特率选择:
# 1 ------------ 9600
# 2 ------------ 115200
GNSS_Baud = 1

# * 参数1：引脚号
#         EC100YCN平台引脚对应关系如下：
#         GPIO1 – 引脚号22
#         GPIO2 – 引脚号23
#         GPIO3 – 引脚号178
#         GPIO4 – 引脚号199
#         GPIO5 – 引脚号204
#
#         EC600SCN平台引脚对应关系如下：
#         GPIO1 – 引脚号12
#         GPIO2 – 引脚号13
#         GPIO3 – 引脚号14
#         GPIO4 – 引脚号15
#         GPIO5 – 引脚号16
# * 参数2：direction
#         IN – 输入模式
#         OUT – 输出模式
# * 参数3：pull
#         PULL_DISABLE – 禁用模式
#         PULL_PU – 上拉模式
#         PULL_PD – 下拉模式
# * 参数4：level
#         0 设置引脚为低电平
#         1 设置引脚为高电平
gpio1 = Pin(Pin.GPIO1, Pin.OUT, Pin.PULL_DISABLE, 0)
if GNSS_Baud == 1:
    uart = UART(UART.UART1, 9600, 8, 0, 1, 0)
elif GNSS_Baud == 2:
    uart = UART(UART.UART1, 115200, 8, 0, 1, 0)
else:
    uart = UART(UART.UART1, 9600, 8, 0, 1, 0)

state = 1
readnum = 500


def uart_read():
    log.debug("uart_read start!")
    global state
    global readnum
    global uart

    # while 1:
    while readnum:
        msgLen = uart.any()  # 返回是否有可读取的数据长度
        if msgLen:  # 当有数据时进行读取
            readnum -= 1
            msg = uart.read(msgLen)
            utf8_msg = msg.decode()  # 初始数据是字节类型（bytes）,将字节类型数据进行编码
            if "Usart End" in utf8_msg:
                break
            else:
                log.info("uart_read msg: {}".format(utf8_msg))
        else:
            utime.sleep_ms(1)
            continue
    state = 0
    log.debug("uart_read end!")


'''
 * 参数1：端口 
        注：EC100YCN平台与EC600SCN平台，UARTn作用如下
        UART0 - DEBUG PORT
        UART1 – BT PORT
        UART2 – MAIN PORT   
        UART3 – USB CDC PORT
 * 参数2：波特率
 * 参数3：data bits  （5~8）
 * 参数4：Parity  （0：NONE  1：EVEN  2：ODD）
 * 参数5：stop bits （1~2）
 * 参数6：flow control （0: FC_NONE  1：FC_HW）
'''


def uart_write():
    global uart

    log.debug("uart_write start!")
    count = 3
    # 配置uart
    while count:
        count -= 1
        utime.sleep(1)

        if GNSS_Baud == 1:
            write_msg = "$PCAS01,5*19\r\n".format(count)  # 115200波特率
        elif GNSS_Baud == 2:
            write_msg = "$PCAS01,1*1D\r\n".format(count)  # 9600波特率
        else:
            write_msg = "$PCAS01,5*19\r\n".format(count)  # 115200波特率
        uart.write(write_msg)  # 发送数据
        log.info("{}".format(write_msg))
        utime.sleep_ms(500)
        uart.close()
        if GNSS_Baud == 1:
            uart = UART(UART.UART1, 115200, 8, 0, 1, 0)
        elif GNSS_Baud == 2:
            uart = UART(UART.UART1, 9600, 8, 0, 1, 0)
        else:
            uart = UART(UART.UART1, 115200, 8, 0, 1, 0)

    log.debug("uart_write end!")


def run():
    log.debug("run start!")
    _thread.start_new_thread(uart_read, ())  # 创建一个线程来监听接收uart消息
    _thread.start_new_thread(uart_write, ())  # 创建一个线程来监听执行uart_write函数
    log.debug("run end!")


if __name__ == "__main__":
    log.debug("main start!")
    # uart_write()
    run()
    while 1:
        if state:
            pass
        else:
            break
    log.debug("main end!")

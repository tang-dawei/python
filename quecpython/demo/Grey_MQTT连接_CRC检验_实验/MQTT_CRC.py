import _thread
import usocket
import utime
# import checkNet
from usr import checkNet
from misc import Power
from machine import RTC
import ustruct as struct
from machine import UART
from machine import Pin
from machine import Timer
from umqtt import MQTTClient
import ujson

# ---------------------------------变量声明区-------------------------------------

CRC16_TABLE = (
    0x0000, 0xC0C1, 0xC181, 0x0140, 0xC301, 0x03C0, 0x0280, 0xC241, 0xC601,
    0x06C0, 0x0780, 0xC741, 0x0500, 0xC5C1, 0xC481, 0x0440, 0xCC01, 0x0CC0,
    0x0D80, 0xCD41, 0x0F00, 0xCFC1, 0xCE81, 0x0E40, 0x0A00, 0xCAC1, 0xCB81,
    0x0B40, 0xC901, 0x09C0, 0x0880, 0xC841, 0xD801, 0x18C0, 0x1980, 0xD941,
    0x1B00, 0xDBC1, 0xDA81, 0x1A40, 0x1E00, 0xDEC1, 0xDF81, 0x1F40, 0xDD01,
    0x1DC0, 0x1C80, 0xDC41, 0x1400, 0xD4C1, 0xD581, 0x1540, 0xD701, 0x17C0,
    0x1680, 0xD641, 0xD201, 0x12C0, 0x1380, 0xD341, 0x1100, 0xD1C1, 0xD081,
    0x1040, 0xF001, 0x30C0, 0x3180, 0xF141, 0x3300, 0xF3C1, 0xF281, 0x3240,
    0x3600, 0xF6C1, 0xF781, 0x3740, 0xF501, 0x35C0, 0x3480, 0xF441, 0x3C00,
    0xFCC1, 0xFD81, 0x3D40, 0xFF01, 0x3FC0, 0x3E80, 0xFE41, 0xFA01, 0x3AC0,
    0x3B80, 0xFB41, 0x3900, 0xF9C1, 0xF881, 0x3840, 0x2800, 0xE8C1, 0xE981,
    0x2940, 0xEB01, 0x2BC0, 0x2A80, 0xEA41, 0xEE01, 0x2EC0, 0x2F80, 0xEF41,
    0x2D00, 0xEDC1, 0xEC81, 0x2C40, 0xE401, 0x24C0, 0x2580, 0xE541, 0x2700,
    0xE7C1, 0xE681, 0x2640, 0x2200, 0xE2C1, 0xE381, 0x2340, 0xE101, 0x21C0,
    0x2080, 0xE041, 0xA001, 0x60C0, 0x6180, 0xA141, 0x6300, 0xA3C1, 0xA281,
    0x6240, 0x6600, 0xA6C1, 0xA781, 0x6740, 0xA501, 0x65C0, 0x6480, 0xA441,
    0x6C00, 0xACC1, 0xAD81, 0x6D40, 0xAF01, 0x6FC0, 0x6E80, 0xAE41, 0xAA01,
    0x6AC0, 0x6B80, 0xAB41, 0x6900, 0xA9C1, 0xA881, 0x6840, 0x7800, 0xB8C1,
    0xB981, 0x7940, 0xBB01, 0x7BC0, 0x7A80, 0xBA41, 0xBE01, 0x7EC0, 0x7F80,
    0xBF41, 0x7D00, 0xBDC1, 0xBC81, 0x7C40, 0xB401, 0x74C0, 0x7580, 0xB541,
    0x7700, 0xB7C1, 0xB681, 0x7640, 0x7200, 0xB2C1, 0xB381, 0x7340, 0xB101,
    0x71C0, 0x7080, 0xB041, 0x5000, 0x90C1, 0x9181, 0x5140, 0x9301, 0x53C0,
    0x5280, 0x9241, 0x9601, 0x56C0, 0x5780, 0x9741, 0x5500, 0x95C1, 0x9481,
    0x5440, 0x9C01, 0x5CC0, 0x5D80, 0x9D41, 0x5F00, 0x9FC1, 0x9E81, 0x5E40,
    0x5A00, 0x9AC1, 0x9B81, 0x5B40, 0x9901, 0x59C0, 0x5880, 0x9841, 0x8801,
    0x48C0, 0x4980, 0x8941, 0x4B00, 0x8BC1, 0x8A81, 0x4A40, 0x4E00, 0x8EC1,
    0x8F81, 0x4F40, 0x8D01, 0x4DC0, 0x4C80, 0x8C41, 0x4400, 0x84C1, 0x8581,
    0x4540, 0x8701, 0x47C0, 0x4680, 0x8641, 0x8201, 0x42C0, 0x4380, 0x8341,
    0x4100, 0x81C1, 0x8081, 0x4040
)
# ----------------------------------config------------------------------------
sysConfig = {'id': '000000000001', 'IP': '47.119.172.183', 'PORT': 1883, 'ReportingPeriod': 5, 'AlarmCycle': 2,
             'turnsRatio': 2000, 'CurrentThreshold': 500, 'mode': 0}

# ---------------------------------UDP---------------------------------------
ID = bytearray([0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x30, 0x31])
isConnect = 0  # 是否链接服务器
rtc = RTC()
Acurrent = [68, 68, 68, 68, 68, 68]  # 电流
Bcurrent = [68, 68, 68, 68, 68, 68]  # 电流
Ccurrent = [68, 68, 68, 68, 68, 68]  # 电流
Avoltage = [3800, 3800, 3800, 3800, 3800, 3800]  # 电压
Bvoltage = [3800, 3800, 3800, 3800, 3800, 3800]  # 电压
Cvoltage = [3800, 3800, 3800, 3800, 3800, 3800]  # 电压
TB = [2902, 2902, 2902, 2902, 2902, 2902]  # 有功功率
EQ = [1000000, 1000000, 1000000, 1000000, 1000000, 1000000]  # 电量
isFristAlarm = 0  # 报警次数
isSendData = 0  # 可以发送数据
dataIndex = 0  # 采集数据序列
# -------------------------MODBUS--------------------------------------------
rs485_rts_pin = Pin(Pin.GPIO1, Pin.OUT, Pin.PULL_DISABLE, 0)
modBusData = bytearray([0x01, 0x03, 0x00, 0x00, 0x00, 0x1D, 0x85, 0xC3])
MODBUS_TXDATA = 0  # 发送数据
MODBUS_RXDATA = 1  # 接收数据
rs485_step = 0  # 等待接收数据标志
isModbusTo = 0  # 接收超时
# -------------------------MQTT----------------------------------------------
CLIENT_ID = b'uMQTT'
SERVER = b'47.119.172.183'
PORT = 1883
USER = None  # b''
PASSWORD = None  # b''

SUB_TOPIC = b"/test"
PUB_TOPIC = SUB_TOPIC

c = None
msgFlag = None
mqttDict = {'id': ID, 'time': rtc.datetime(), 'Avoltage': 380, 'Bvoltage': 380, 'Cvoltage': 380, 'Acurrent': 10,
            'Bcurrent': 20, 'Ccurrent': 30, 'TB': 2902, 'EQ': 1000000}
a = None
b = None
amsgFlag = None
bmsgFlag = None


# -------------------------函数声明区-----------------------------------------

# CRC16计算
def _calculate_crc16(data):
    crc = 0xFFFF

    for char in data:
        crc = (crc >> 8) ^ CRC16_TABLE[(crc ^ char) & 0xFF]

    return struct.pack('>H', crc)


# 打印十六进制数
def print_hex(byte):
    l = [hex(int(i)) for i in byte]
    print(" ".join(l))


# ----------------------------------MQTT-------------------------------------
def mqtt_pack():
    global Avoltage  # 电压
    global Bvoltage  # 电压
    global Cvoltage  # 电压
    global Acurrent  # 电流
    global Bcurrent  # 电流
    global Ccurrent  # 电流
    global TB  # 有功功率
    global EQ  # 电量
    global mqttDict
    mqttDict['Avoltage'] = Avoltage[0]
    mqttDict['Bvoltage'] = Bvoltage[0]
    mqttDict['Cvoltage'] = Cvoltage[0]
    mqttDict['Acurrent'] = Acurrent[0]
    mqttDict['Bcurrent'] = Bcurrent[0]
    mqttDict['Ccurrent'] = Ccurrent[0]
    mqttDict['TB'] = TB[0]
    mqttDict['EQ'] = EQ[0]


def MQTT_Init():
    global c
    global a
    # global b

    # 创建一个mqtt实例
    c = MQTTClient(
        client_id=CLIENT_ID,
        server=SERVER,
        port=PORT,
        user=USER,
        password=PASSWORD,
        keepalive=30)  # 必须要 keepalive=30 ,否则连接不上
    # 设置消息回调
    c.set_callback(sub_cb)
    # 建立连接
    c.connect()
    # 订阅主题
    c.subscribe(SUB_TOPIC)
    # 发布消息
    c.publish(PUB_TOPIC, "test publish 你好")
    a = MQTTClient(
        client_id=CLIENT_ID,
        server=SERVER,
        port=PORT,
        user=USER,
        password=PASSWORD,
        keepalive=30)  # 必须要 keepalive=30 ,否则连接不上
    # 设置消息回调
    a.set_callback(asub_cb)
    # 建立连接
    a.connect()
    # 订阅主题
    a.subscribe(b"/alarm")
    # 发布消息
    a.publish(b"/alarm", "alarm publish 你好~")
    # b = MQTTClient(
    # client_id=CLIENT_ID,
    # server=SERVER,
    # port=PORT,
    # user=USER,
    # password=PASSWORD,
    # keepalive=30)  # 必须要 keepalive=30 ,否则连接不上
    # 设置消息回调
    # b.set_callback(bsub_cb)
    # 建立连接
    # b.connect()
    # 订阅主题
    # b.subscribe(b"/setting")
    # 发布消息
    # b.publish(b"/setting", "setting publish 你好~")


def sub_cb(topic, msg):
    global c
    global msgFlag
    print(
        "Subscribe Recv: Topic={},Msg={}".format(
            topic.decode(),
            msg.decode()))
    msgFlag = True


def wait_msg():
    global c
    while True:
        if c is not None:
            print('c.wait_msg()')
            c.wait_msg()
            utime.sleep_ms(1000)


def MQTT_PUB():
    global c
    global msgFlag
    global mqttDict
    while True:
        if c is not None:
            if msgFlag:
                msgFlag = False
                mqtt_pack()
                c.publish(PUB_TOPIC, ujson.dumps(mqttDict))
            utime.sleep_ms(1000)  # 按照周期上传打包数据


def asub_cb(topic, msg):
    global a
    global amsgFlag
    print(
        "Subscribe Recv: Topic={},Msg={}".format(
            topic.decode(),
            msg.decode()))
    amsgFlag = True


def await_msg():
    global a
    while True:
        if a is not None:
            print('a.wait_msg()')
            a.wait_msg()
            utime.sleep_ms(2000)


def aMQTT_PUB():
    global a
    global amsgFlag
    while True:
        if a is not None:
            if amsgFlag:
                amsgFlag = False
                a.publish(b"/alarm", "alarm publish 你好~")
            utime.sleep_ms(2000)  # 按照周期上传打包数据


def bsub_cb(topic, msg):
    global b
    global bmsgFlag
    print(
        "Subscribe Recv: Topic={},Msg={}".format(
            topic.decode(),
            msg.decode()))
    bmsgFlag = True


def bwait_msg():
    global b
    while True:
        if b is not None:
            print('b.wait_msg()')
            b.wait_msg()
            utime.sleep_ms(3000)


def bMQTT_PUB():
    global b
    global bmsgFlag
    while True:
        if b is not None:
            if bmsgFlag:
                bmsgFlag = False
                b.publish(PUB_TOPIC, "setting publish 你好~")
            utime.sleep_ms(3000)  # 按照周期上传打包数据


# ----------------------------------config-------------------------------------
# 参数初始化
def para_Init():
    global sysConfig
    if (os.path.exists("/usr/myconfig.json") == True):  # 如果有文件存在，读取参数
        with open("/usr/myconfig.json", "r") as fo:
            sysConfig = json.load(fo)  # 读取参数
    else:  # 如果没有文件存在，将配置参数写入
        with open("/usr/myconfig.json", "x") as fo:
            json.dump(sysConfig, fo)  # 写入默认参数


# 参数保存
def para_save():
    global sysConfig
    with open("/usr/myconfig.json", "w") as fo:
        json.dump(sysConfig, fo)


# 协议解析
def para_analysis(src):
    global sysConfig
    data = src
    if (data.find('\r\n') == -1):  # 检查是否合法
        return 0
    if (data.find('AT+ID=') != -1):
        sysConfig['id'] = data[data.find('=') + 1:-2]
        return 1
    elif (data.find('AT+IP=') != -1):
        sysConfig['IP'] = data[data.find('=') + 1:-2]
        return 1
    elif (data.find('AT+PORT=') != -1):
        sysConfig['PORT'] = int(data[data.find('=') + 1:-2])
        return 1
    elif (data.find('AT+ReportingPeriod=') != -1):
        sysConfig['ReportingPeriod'] = int(data[data.find('=') + 1:-2])
        return 1
    elif (data.find('AT+AlarmCycle=') != -1):
        sysConfig['AlarmCycle'] = int(data[data.find('=') + 1:-2])
        return 1
    elif (data.find('AT+turnsRatio=') != -1):
        sysConfig['turnsRatio'] = int(data[data.find('=') + 1:-2])
        return 1
    elif (data.find('AT+CurrentThreshold=') != -1):
        sysConfig['CurrentThreshold'] = int(data[data.find('=') + 1:-2])
        return 1
    elif (data.find('AT+mode=') != -1):
        sysConfig['CurrentThreshold'] = int(data[data.find('=') + 1:-2])
        return 1
    else:
        return 0


def para_dataRev():  # 数据接收
    uart = UART(UART.UART0, 9600, 8, 0, 1, 0)
    while True:
        msgLen = uart.any()
        # 当有数据时进行读取
        if msgLen:
            print('接收到配置参数')
            data = uart.read(msgLen)
            print(data)
            if (para_analysis(data)):
                para_save()  # 参数保存
                uart.write('OK\r\n')  # 发送回复报文
        utime.sleep_ms(100)  # 每隔100ms扫描一次


# ----------------------------------主函数-------------------------------------
if __name__ == '__main__':
    '''
    下面两个全局变量是必须有的，用户可以根据自己的实际项目修改下面两个全局变量的值，
    在执行用户代码前，会先打印这两个变量的值。
    '''
    PROJECT_NAME = "QuecPython"
    PROJECT_VERSION = "1.0.0"

    checknet = checkNet.CheckNetwork(PROJECT_NAME, PROJECT_VERSION)

    '''
     手动运行本例程时，可以去掉该延时，如果将例程文件名改为main.py，希望开机自动运行时，需要加上该延时,
     否则无法从CDC口看到下面的 poweron_print_once() 中打印的信息
     '''
    # utime.sleep(5)
    checknet.poweron_print_once()
    '''
    如果用户程序包含网络相关代码，必须执行 wait_network_connected() 等待网络就绪（拨号成功）；
    如果是网络无关代码，可以屏蔽 wait_network_connected()
    【本例程必须保留下面这一行！】
    '''
    try:
        checknet.wait_network_connected()
    except BaseException:
        print('Not Net, Resatrting...')
        utime.sleep_ms(200)
        Power.powerRestart()

    # para_Init()  #参数初始化
    MQTT_Init()  # MQTT初始化

    _thread.start_new_thread(MQTT_PUB, ())
    _thread.start_new_thread(wait_msg, ())
    # _thread.start_new_thread(para_dataRev, ())  #参数配置进程
    _thread.start_new_thread(aMQTT_PUB, ())
    _thread.start_new_thread(await_msg, ())
    # _thread.start_new_thread(bMQTT_PUB, ())
    # _thread.start_new_thread(bwait_msg, ())

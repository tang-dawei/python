import log
from machine import SPI
import utime
import _thread
import ubinascii

# from usr import globalPage


# MF522命令字
PCD_IDLE = 0x00  # 取消当前命令
PCD_AUTHENT = 0x0E  # 验证密钥
PCD_RECEIVE = 0x08  # 接收数据
PCD_TRANSMIT = 0x04  # 发送数据
PCD_TRANSCEIVE = 0x0C  # 发送并接收数据
PCD_RESETPHASE = 0x0F  # 复位
PCD_CALCCRC = 0x03  # CRC计算

# Mifare_One卡片命令字

PICC_REQIDL = 0x26  # 寻天线区内未进入休眠状态
PICC_REQALL = 0x52  # 寻天线区内全部卡
PICC_ANTICOLL1 = 0x93  # 防冲撞
PICC_ANTICOLL2 = 0x95  # 防冲撞
PICC_AUTHENT1A = 0x60  # 验证A密钥
PICC_AUTHENT1B = 0x61  # 验证B密钥
PICC_READ = 0x30  # 读块
PICC_WRITE = 0xA0  # 写块
PICC_DECREMENT = 0xC0  # 扣款
PICC_INCREMENT = 0xC1  # 充值
PICC_RESTORE = 0xC2  # 调块数据到缓冲区
PICC_TRANSFER = 0xB0  # 保存缓冲区中数据
PICC_HALT = 0x50  # 休眠

# MF522 FIFO长度定义
DEF_FIFO_LENGTH = 64  # FIFO size=64byte
MAXRLEN = 18

# MF522寄存器定义
# PAGE 0
RFU00 = 0x00
CommandReg = 0x01
ComIEnReg = 0x02
DivlEnReg = 0x03
ComIrqReg = 0x04
DivIrqReg = 0x05
ErrorReg = 0x06
Status1Reg = 0x07
Status2Reg = 0x08
FIFODataReg = 0x09
FIFOLevelReg = 0x0A
WaterLevelReg = 0x0B
ControlReg = 0x0C
BitFramingReg = 0x0D
CollReg = 0x0E
RFU0F = 0x0F

# PAGE 1
RFU10 = 0x10
ModeReg = 0x11
TxModeReg = 0x12
RxModeReg = 0x13
TxControlReg = 0x14
TxAutoReg = 0x15
TxSelReg = 0x16
RxSelReg = 0x17
RxThresholdReg = 0x18
DemodReg = 0x19
RFU1A = 0x1A
RFU1B = 0x1B
MifareReg = 0x1C
RFU1D = 0x1D
RFU1E = 0x1E
SerialSpeedReg = 0x1F

# PAGE 2
RFU20 = 0x20
CRCResultRegM = 0x21
CRCResultRegL = 0x22
RFU23 = 0x23
ModWidthReg = 0x24
RFU25 = 0x25
RFCfgReg = 0x26
GsNReg = 0x27
CWGsCfgReg = 0x28
ModGsCfgReg = 0x29
TModeReg = 0x2A
TPrescalerReg = 0x2B
TReloadRegH = 0x2C
TReloadRegL = 0x2D
TCounterValueRegH = 0x2E
TCounterValueRegL = 0x2F

# PAGE 3
RFU30 = 0x30
TestSel1Reg = 0x31
TestSel2Reg = 0x32
TestPinEnReg = 0x33
TestPinValueReg = 0x34
TestBusReg = 0x35
AutoTestReg = 0x36
VersionReg = 0x37
AnalogTestReg = 0x38
TestDAC1Reg = 0x39
TestDAC2Reg = 0x3A
TestADCReg = 0x3B
RFU3C = 0x3C
RFU3D = 0x3D
RFU3E = 0x3E
RFU3F = 0x3F

# 和MF522通讯时返回的错误代码
MI_OK = 0
MI_NOTAGERR = 1
MI_ERR = 2

SHAQU1 = 0X01
KUAI4 = 0X04
KUAI7 = 0X07
REGCARD = 0xa1
CONSUME = 0xa2
READCARD = 0xa3
ADDMONEY = 0xa4


'''
SPI使用示例
适配版本：EC100Y(V0009)及以上；EC600S(V0002)及以上
'''

'''
下面两个全局变量是必须有的，用户可以根据自己的实际项目修改下面两个全局变量的值
'''
PROJECT_NAME = "QuecPython_SPI_example"
PROJECT_VERSION = "1.0.0"

# 设置日志输出级别
log.basicConfig(level=log.INFO)
spi_log = log.getLogger("SPI")

spi_obj = SPI(1, 0, 0)


card_check = ''


# 读取寄存器的值
def ReadRawRC(address):

    r_data = bytearray(2)  # 创建接收数据的buff
    ucAddr = (address << 1) & 0x7E | 0x80
    # data = chr(ucAddr)

    # spi_log.info(ucAddr)

    # spi_log.info(hex(ucAddr))
    # spi_log.info(hex(ucAddr).encode())
    # spi_log.info(data)
    # spi_log.info(len(data))
    # spi_log.info(bytes(ucAddr))
    spi_obj.write_read(r_data, bytearray([ucAddr]), 2)
    # spi_log.info(r_data)
    # spi_log.info(r_data[1])
    return r_data[1]

# 写寄存器的值


def WriteRawRC(address, value):

    # r_data = bytearray(1)  # 创建接收数据的buff
    ucAddr = (address << 1) & 0x7E
    # data = chr(ucAddr)
    spi_obj.write(bytearray([ucAddr, value]), 2)
    # spi_obj.write(chr(value), 1)

    # temp = ReadRawRC(address)
    # spi_log.info('WriteRawRC')
    # spi_log.info(hex(address))
    # spi_log.info(hex(value))
    # spi_log.info(hex(temp))

    # if temp != int(value):
    #     spi_log.info('write error')


# 置RC522寄存器位
def SetBitMask(reg, mask):

    tmp = ReadRawRC(reg)
    # spi_log.info('SetBitMask')
    # spi_log.info(tmp)
    WriteRawRC(reg, tmp | mask)


# 清RC522寄存器位
def ClearBitMask(reg, mask):

    tmp = ReadRawRC(reg)
    # spi_log.info('ClearBitMask')
    # spi_log.info(tmp)
    WriteRawRC(reg, tmp & ~mask)


# 开启天线
def PcdAntennaOn():
    i = ReadRawRC(TxControlReg)
    if i & 0x03 == 0:
        SetBitMask(TxControlReg, 0x03)


# 关闭天线
def PcdAntennaOff():
    ClearBitMask(TxControlReg, 0x03)


def PcdReset():
    WriteRawRC(CommandReg, PCD_RESETPHASE)

    WriteRawRC(CommandReg, PCD_RESETPHASE)
    # delay_us(10)
    utime.sleep_us(10)

    WriteRawRC(ModeReg, 0x3D)
    # //和Mifare卡通讯，CRC初始值0x6363
    WriteRawRC(TReloadRegL, 30)
    WriteRawRC(TReloadRegH, 0)
    WriteRawRC(TModeReg, 0x8D)
    WriteRawRC(TPrescalerReg, 0x3E)

    WriteRawRC(TxAutoReg, 0x40)

    return MI_OK


# 功    能：寻卡
# 参数说明: req_code[IN]: 寻卡方式
# 0x52 = 寻感应区内所有符合14443A标准的卡
# 0x26 = 寻未进入休眠状态的卡
# pTagType[OUT]：卡片类型代码
# 0x4400 = Mifare_UltraLight
# 0x0400 = Mifare_One(S50)
# 0x0200 = Mifare_One(S70)
# 0x0800 = Mifare_Pro(X)
# 0x4403 = Mifare_DESFire
# 返    回: 成功返回MI_OK

def PcdRequest(req_code, pTagType):
    ucComMF522Buf = [0]*MAXRLEN

    ClearBitMask(Status2Reg, 0x08)
    WriteRawRC(BitFramingReg, 0x07)
    SetBitMask(TxControlReg, 0x03)

    # print('Status2Reg', hex(ReadRawRC(Status2Reg)))
    # print('TxControlReg', hex(ReadRawRC(TxControlReg)))

    ucComMF522Buf[0] = req_code

    status, unLen = PcdComMF522(
        PCD_TRANSCEIVE, ucComMF522Buf, 1, ucComMF522Buf)

    # print('ucComMF522Buf', ucComMF522Buf)

    if status == MI_OK and unLen == 0x10:
        pTagType[0] = ucComMF522Buf[0]
        pTagType[1] = ucComMF522Buf[1]

    else:
        status = MI_ERR

    return status


# 功    能：防冲撞
# 参数说明: pSnr[OUT]: 卡片序列号，4字节
# 返    回: 成功返回MI_OK

def PcdAnticoll(pSnr):

    ucComMF522Buf = [0]*MAXRLEN
    snr_check = 0
    # i = 0
    ClearBitMask(Status2Reg, 0x08)
    WriteRawRC(BitFramingReg, 0x00)
    ClearBitMask(CollReg, 0x80)

    ucComMF522Buf[0] = PICC_ANTICOLL1
    ucComMF522Buf[1] = 0x20

    status, unLen = PcdComMF522(
        PCD_TRANSCEIVE, ucComMF522Buf, 2, ucComMF522Buf)

    if status == MI_OK:
        for i in range(4):
            pSnr[i] = ucComMF522Buf[i]
            snr_check ^= ucComMF522Buf[i]

        # spi_log.info('pSnr')
        # spi_log.info(pSnr)

        # spi_log.info('snr_check')
        # spi_log.info(snr_check)
        # spi_log.info(ucComMF522Buf[4])

        if snr_check != ucComMF522Buf[4]:
            status = MI_ERR

    SetBitMask(CollReg, 0x80)
    return status

# 功    能：选定卡片
# 参数说明: pSnr[IN]: 卡片序列号，4字节
# 返    回: 成功返回MI_OK


def PcdSelect(pSnr):

    ucComMF522Buf = [0]*MAXRLEN
    ucComMF522Buf[0] = PICC_ANTICOLL1
    ucComMF522Buf[1] = 0x70
    ucComMF522Buf[6] = 0

    for i in range(4):
        ucComMF522Buf[i+2] = pSnr[i]
        ucComMF522Buf[6] ^= pSnr[i]

    CalulateCRC(ucComMF522Buf, 7, ucComMF522Buf, 7)

    ClearBitMask(Status2Reg, 0x08)

    status, unLen = PcdComMF522(
        PCD_TRANSCEIVE, ucComMF522Buf, 9, ucComMF522Buf)

    if status == MI_OK and unLen == 0x18:
        status == MI_OK
    else:
        status = MI_ERR

    return status


# 用MF522计算CRC16函数
def CalulateCRC(pIn, len,  pOut, bit):
    ClearBitMask(DivIrqReg, 0x04)
    WriteRawRC(CommandReg, PCD_IDLE)
    SetBitMask(FIFOLevelReg, 0x80)
    for i in range(len):
        WriteRawRC(FIFODataReg, pIn[i])
    WriteRawRC(CommandReg, PCD_CALCCRC)
    i = 0xFF
    # n = 0
    while (True):
        n = ReadRawRC(DivIrqReg)
        i = i-1

        if not((i != 0) and not(n & 0x04)):
            break

    pOut[bit] = ReadRawRC(CRCResultRegL)
    pOut[bit+1] = ReadRawRC(CRCResultRegM)


def PcdHalt():

    ucComMF522Buf = [0]*MAXRLEN

    ucComMF522Buf[0] = PICC_HALT
    ucComMF522Buf[1] = 0
    CalulateCRC(ucComMF522Buf, 2, ucComMF522Buf, 0)

    status, len = PcdComMF522(PCD_TRANSCEIVE, ucComMF522Buf, 4, ucComMF522Buf)

    # spi_log.info('PcdHalt')
    # spi_log.info(status)
    # spi_log.info(ucComMF522Buf)

    return status


# 设置RC632的工作方式
def M500PcdConfigISOType(type):

    if type == 'A':
        ClearBitMask(Status2Reg, 0x08)
        WriteRawRC(ModeReg, 0x3D)
        WriteRawRC(RxSelReg, 0x86)
        WriteRawRC(RFCfgReg, 0x7F)
        WriteRawRC(TReloadRegL, 30)
        WriteRawRC(TReloadRegH, 0)
        WriteRawRC(TModeReg, 0x8D)
        WriteRawRC(TPrescalerReg, 0x3E)
        utime.sleep_us(1000)
        PcdAntennaOn()
    else:
        return 1

    return MI_OK


# RC522初始化
def Init_RC522():
    spi_log.info(
        '---------------------------------------------------初始化RC522------------------------------------------------------------')

    utime.sleep_ms(100)
    PcdReset()
    PcdAntennaOff()
    PcdAntennaOn()
    M500PcdConfigISOType('A')


# 功    能：通过RC522和ISO14443卡通讯
# 参数说明：Command[IN]: RC522命令字
# pIn[IN]: 通过RC522发送到卡片的数据
# InLenByte[IN]: 发送数据的字节长度
# pOut[OUT]: 接收到的卡片返回数据
def PcdComMF522(Command, pIn, InLenByte, pOut):

    status = MI_ERR

    pOutLenBit = 0
    n = 0

    if Command == PCD_AUTHENT:
        irqEn = 0x12
        waitFor = 0x10
    elif Command == PCD_TRANSCEIVE:
        irqEn = 0x77
        waitFor = 0x30
    else:
        pass

    # spi_log.info('waitFor=')
    # spi_log.info(waitFor)
    # spi_log.info(pIn)

    ClearBitMask(ComIrqReg, 0x80)  # //清所有中断位
    WriteRawRC(ComIEnReg, irqEn | 0x80)
    WriteRawRC(CommandReg, PCD_IDLE)
    SetBitMask(FIFOLevelReg, 0x80)  # //清FIFO缓存

    for i in range(InLenByte):
        WriteRawRC(FIFODataReg, pIn[i])

    WriteRawRC(CommandReg, Command)

    if Command == PCD_TRANSCEIVE:
        SetBitMask(BitFramingReg, 0x80)

    i = 600

    # i = 10000

    while True:
        n = ReadRawRC(ComIrqReg)
        i = i-1
        if not((i != 0) and not(n & 0x01) and not(n & waitFor)):
            break

    ClearBitMask(BitFramingReg, 0x80)
    # if InLenByte == 2:
    #     spi_log.info('i=')
    #     spi_log.info(i)
    #     spi_log.info('n=')
    #     spi_log.info(hex(n))
    if i != 0:
        errReg = ReadRawRC(ErrorReg)
        # spi_log.info('ReadRawRC(ErrorReg)=')
        # spi_log.info(errReg)

        # spi_log.info('errReg & 0x1B=')
        # spi_log.info(errReg & 0x1B)
        if not (errReg & 0x1B):

            status = MI_OK
            if n & irqEn & 0x01:
                status = MI_NOTAGERR
            if Command == PCD_TRANSCEIVE:
                n = ReadRawRC(FIFOLevelReg)
                ctrlReg = ReadRawRC(ControlReg)
                lastBits = ctrlReg & 0x07

                # if InLenByte == 2:
                #     spi_log.info('ReadRawRC(FIFOLevelReg)=')
                #     spi_log.info(n)

                # spi_log.info('ReadRawRC(ControlReg)=')
                # spi_log.info(ctrlReg)
                if lastBits:
                    pOutLenBit = (n-1)*8 + lastBits
                else:
                    pOutLenBit = n*8
                if n == 0:
                    n = 1
                if n > MAXRLEN:
                    n = MAXRLEN
                for i in range(n):
                    pOut[i] = ReadRawRC(FIFODataReg)
        else:
            status = MI_ERR
    # if InLenByte == 2:
    #     spi_log.info('status=')
    #     spi_log.info(status)

    #     spi_log.info('pOut=')
    #     spi_log.info(pOut)

    #     spi_log.info('ulen=')
    #     spi_log.info(pOutLenBit)
    SetBitMask(ControlReg, 0x80)  # stop timer now
    WriteRawRC(CommandReg, PCD_IDLE)
    return (status, pOutLenBit)

# 读取卡号


def read_rfid():

    CT = [0, 0]
    rfid = [0, 0, 0, 0]
    # cardId = ""
    # 寻卡
    if PcdRequest(PICC_REQALL, CT) != MI_OK:

        # spi_log.info('PcdRequest(PICC_REQALL, CT) != MI_OK')
        # spi_log.info('globalPage.getCardtype=')
        # spi_log.info(globalPage.getCardtype())
        # spi_log.info('portList=')
        # spi_log.info(globalPage.getWholePortList())
        # spi_log.info(CT)
        return 0
    # spi_log.info('findcardsuccess')
    # spi_log.info(CT)
    # 防冲撞
    if PcdAnticoll(rfid) != MI_OK:
        # spi_log.info('PcdAnticoll(rfid) != MI_OK')

        return 0
    # spi_log.info('Anticollsuccess')

    # spi_log.info(rfid)
    # 选卡
    if PcdSelect(rfid) != MI_OK:
        # spi_log.info('PcdSelect(rfid) != MI_OK')
        # spi_log.info(rfid)
        return 0

    # spi_log.info('PcdSelectsuccess')
    PcdHalt()
    PcdReset()
    # spi_log.info(rfid)

    # for i in range(4):
    cardId = "%X%X%X%X" % (rfid[0], rfid[1], rfid[2], rfid[3])
    # spi_log.info(cardId)

    return cardId


def SPI_Read():
    Init_RC522()
    spi_log.info(
        '-------------------------------------------初始化完毕-------------------------------------------------------')

    # utime.sleep_ms(1000)
    count = 50
    while True:

        # spi_log.info(
        #     '---------------------------------------------------------------------------------------------------------------')

        cardId = read_rfid()
        if(cardId):
            spi_log.info(cardId)

        # spi_log.info(res2)
        utime.sleep_ms(2000)
        # count = count-1

    # ret = spi_obj.write_read(r_data, data, 256)  # 写入数据并接收


if __name__ == '__main__':
    _thread.start_new_thread(SPI_Read, ())
    # _thread.start_new_thread(testInit, ())

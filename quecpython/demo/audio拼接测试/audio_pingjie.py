#先把拼接需要的音频文件：1.amr到10.amr下载到模组的usr目录下或者新建usr/media目录下
#运行下面代码
import audio
aud = audio.Audio(0)
#必须要以"xxxfiles="开头，如果播放amr文件的话，则命名为"amrfiles=",见下面例子；如果播放mp3文件，则命名为"mp3files="
usr_amr_test = 'amrfiles=U:/+1.amr+2.amr+3.amr+4.amr+5.amr+6.amr+7.amr+8.amr+9.amr+0.amr'
usr_media_amr_test = 'amrfiles=U:/media+1.amr+2.amr+3.amr+4.amr+5.amr+6.amr+7.amr+8.amr+9.amr+0.amr'
aud.play(2,0,usr_amr_test)
aud.play(2,0,usr_media_amr_test)
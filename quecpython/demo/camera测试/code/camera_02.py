from machine import LCD                 # 屏幕显示模块
import utime
import camera
import _thread
import qrcode


LCD_SIZE_H = 240	# 屏高
LCD_SIZE_W = 240	# 屏高
XSTART_H = 0xf0		# 起始点X坐标高字节寄存器
XSTART_L = 0xf1		# 起始点X坐标低字节寄存器
XEND_H = 0xE0		# 结束点X坐标高字节寄存器
XEND_L = 0xE1		# 结束点X坐标低字节寄存器
YSTART_H = 0xf2		# 起始点Y坐标高字节寄存器
YSTART_L = 0xf3		# 起始点Y坐标低字节寄存器
YEND_H = 0xE2		# 结束点Y坐标高字节寄存器
YEND_L = 0xE3		# 结束点Y坐标低字节寄存器
lcd = LCD()	        # 创建对象
init_data = (
0, 0, 0x11,0, 1, 0x36,1, 1, 0x00,0, 1, 0x3A,1, 1, 0x05,0, 0, 0x21,0, 5, 0xB2,1, 1, 0x05,1, 1, 0x05,1, 1, 0x00,1, 1, 0x33,1, 1, 0x33,
0, 1, 0xB7,1, 1, 0x23,0, 1, 0xBB,1, 1, 0x22,0, 1, 0xC0,1, 1, 0x2C,0, 1, 0xC2,1, 1, 0x01,0, 1, 0xC3,1, 1, 0x13,0, 1, 0xC4,1, 1, 0x20,
0, 1, 0xC6,1, 1, 0x0F,0, 2, 0xD0,1, 1, 0xA4,1, 1, 0xA1,0, 1, 0xD6,1, 1, 0xA1,
0, 14, 0xE0,1, 1, 0x70,1, 1, 0x06,1, 1, 0x0C,1, 1, 0x08,1, 1, 0x09,1, 1, 0x27,1, 1, 0x2E,1, 1, 0x34,1, 1, 0x46,1, 1, 0x37,1, 1, 0x13,1, 1, 0x13,1, 1, 0x25,1, 1, 0x2A,
0, 14, 0xE1,1, 1, 0x70,1, 1, 0x04,1, 1, 0x08,1, 1, 0x09,1, 1, 0x07,1, 1, 0x03,1, 1, 0x2C,1, 1, 0x42,1, 1, 0x42,1, 1, 0x38,1, 1, 0x14,1, 1, 0x14,1, 1, 0x27,1, 1, 0x2C,
0, 0, 0x29,0, 1, 0x36,1, 1, 0x00,0, 4, 0x2a,1, 1, 0x00,1, 1, 0x00,1, 1, 0x00,1, 1, 0xef,0, 4, 0x2b,1, 1, 0x00,1, 1, 0x00,1, 1, 0x00,1, 1, 0xef,0, 0, 0x2c,
)
lcd_set_display_area = (
0, 4, 0x2a,1, 1, XSTART_H,1, 1, XSTART_L,1, 1, XEND_H,1, 1, XEND_L,
0, 4, 0x2b,1, 1, YSTART_H,1, 1, YSTART_L,1, 1, YEND_H,1, 1, YEND_L,0, 0, 0x2c,
)

lcd_init_data = bytearray(init_data)			                                                                            # 转换初始化参数数组
lcd_invalid = bytearray(lcd_set_display_area)                                                                           	# 转换区域设定参数数组
lcd.lcd_init(lcd_init_data, LCD_SIZE_H, LCD_SIZE_W, 13000, 1, 4, 0, lcd_invalid, None, None, None)							# 初始化LCD屏
utime.sleep_ms(2000)
lcd.lcd_clear(0xF81F)						                                                                                # 清屏,设置成紫色
utime.sleep_ms(1000)
def callback(para):                                                                                                         # 配置扫码回调
    global para_content,scan_flag
    para_content = para
    print(para_content)                                                                                                     # 打印扫码信息
    scan_flag = 0
scan = camera.camScandecode(0,1,640,480,1,240,240)                                                                          # 配置摄像头扫码
scan.callback(callback) 
scan_flag = 1                                                                                                               # 配置扫码标志
def scan_qrcode():
    global scan,para_content,scan_flag
    if scan_flag == 1:                                                                                                      # 开始扫码
        scan.open() 
        scan.start()
        utime.sleep_ms(100)
    else:                                                                                                                   # 否则显示扫描的二维码
        scan.stop()
        scan.close()
        utime.sleep_ms(1000)
        qrcode.show(para_content[1],4,60,60)
        utime.sleep_ms(1000)
        scan_flag = 1

if __name__ == "__main__": 
    while True:
        scan_qrcode()
        utime.sleep_ms(1000)


